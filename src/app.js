const express = require('express');
const winston = require('winston');
const expressWinston = require('express-winston');
const app = express();
const port = 3000;

app.use(expressWinston.logger({
  transports: [
    new winston.transports.Console()
  ],
}));

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/products', (req, res) => {
  res.json({
    data: [
      {
        id: 1,
        sku: 'A001',
        name: 'Apple iPad 2020 64GB'
      },
      {
        id: 2,
        sku: 'A002',
        name: 'Apple iPhone 10 128GB'
      }
    ]
  })
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));