FROM node:10

WORKDIR /app

RUN mkdir -p /app

COPY ./package*.json ./
COPY ./src .

RUN npm install

ENTRYPOINT [ "node", "/app/app.js" ]